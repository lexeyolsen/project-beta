from nturl2path import url2pathname
from django.urls import path

from .views import api_list_autos, api_list_sales_record, api_list_sales_people, api_list_potential_customers, api_list_unsold_autos, api_show_potential_customer, api_show_sales_by_seller


urlpatterns = [
    path("sales_people/", api_list_sales_people, name="api_list_sales_people"),
    path("potential_customers/", api_list_potential_customers, name="api_list_potential_customers"),
    path("potential_customer/<int:pk>/", api_show_potential_customer, name="api_show_potential_customer"),
    path("list_sales/", api_list_sales_record, name="api_list_sales"),
    path("list_autos/", api_list_autos, name="api_list_autos"),
    path("list_unsold_autos/", api_list_unsold_autos, name="api_list_unsold_autos"),
    path("list_sales/<int:pk>/", api_show_sales_by_seller, name="api_show_sales_by_seller"),
]