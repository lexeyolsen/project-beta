import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

async function loadComponets() {
  let sales, services, manufacturers, models, autos;
  const response = await fetch("http://localhost:8090/api/list_sales/");
  const response2 = await fetch(
    "http://localhost:8080/api/unfinished_services/"
  );
  const response3 = await fetch("http://localhost:8100/api/manufacturers/");
  const response4 = await fetch("http://localhost:8100/api/models/");
  const response5 = await fetch("http://localhost:8100/api/automobiles/");

  if (response.ok) {
    const data = await response.json();
    sales = data.sales;
  } else {
    console.error("failed to load", response);
  }

  if (response2.ok) {
    const data2 = await response2.json();
    services = data2.services;
  } else {
    console.error("failed to load", response2);
  }

  if (response3.ok) {
    const data3 = await response3.json();
    manufacturers = data3.manufacturers;
  } else {
    console.error("failed to load", response3);
  }

  if (response4.ok) {
    const data4 = await response4.json();
    models = data4.models;
  } else {
    console.error("failed to load", response4);
  }

  if (response5.ok) {
    const data5 = await response5.json();
    autos = data5.autos;
  } else {
    console.error("failed to load", response5);
  }

  root.render(
    <React.StrictMode>
      <App
        sales={sales}
        services={services}
        manufacturers={manufacturers}
        models={models}
        automobiles={autos}
      />
    </React.StrictMode>
  );
}
loadComponets();
