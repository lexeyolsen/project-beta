import React from "react";

class SalesPersonForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      number: "",
    };
    this.handleNameChange = this.handleNameChange.bind(this);
    this.handleNumberChange = this.handleNumberChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleNameChange(event) {
    const value = event.target.value;
    this.setState({ name: value });
  }

  handleNumberChange(event) {
    const value = event.target.value;
    this.setState({ number: value });
  }

  async handleSubmit(event) {
    event.preventDefault();
    const data = { ...this.state };

    const salesPersonUrl = "http://localhost:8090/api/sales_people/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(salesPersonUrl, fetchConfig);
    if (response.ok) {
    }
    const cleared = {
      name: "",
      number: "",
    };
    this.setState(cleared);
  }

  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>New Sales Person</h1>
            <form onSubmit={this.handleSubmit} id="create-sales-person">
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleNameChange}
                  value={this.state.name}
                  placeholder="Name"
                  required
                  type="text"
                  name="name"
                  id="name"
                  className="form-control"
                />
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleNumberChange}
                  value={this.state.number}
                  placeholder="Number"
                  required
                  type="text"
                  name="number"
                  id="number"
                  className="form-control"
                />
                <label htmlFor="number">Employee Number</label>
              </div>
              <button className="btn btn-outline-success">
                Create Sales Person
              </button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default SalesPersonForm;
