import React from "react";

class SalesBySeller extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      sellers: [],
      sales: [],
      seller: "",
    };
    this.handleSellerChange = this.handleSellerChange.bind(this);
  }

  async handleSellerChange(event) {
    const value = event.target.value;

    const sellerUrl = `http://localhost:8090/api/list_sales/${value}/`;

    const sellerHistoryResponse = await fetch(sellerUrl);

    if (sellerHistoryResponse.ok) {
      const sellerHistoryData = await sellerHistoryResponse.json();

      this.setState({ sales: sellerHistoryData.sales });
    }
  }

  async componentDidMount() {
    const sellersUrl = "http://localhost:8090/api/sales_people/";
    const sellerResponse = await fetch(sellersUrl);

    if (sellerResponse.ok) {
      const sellerData = await sellerResponse.json();

      this.setState({ sellers: sellerData.sellers });
    }
  }

  render() {
    return (
      <>
        <div className="mt-4 ">
          <h2>Seller History</h2>
        </div>
        <div className="row">
          <div className="mb-4">
            <form className="d-flex">
              <select
                onChange={this.handleSellerChange}
                value={this.state.seller.id}
                name="seller"
                id="seller"
                className="form-select me-2 "
              >
                <option value="">Please Choose a Seller</option>
                {this.state.sellers.map((seller) => {
                  return (
                    <option key={seller.id} value={seller.id}>
                      {seller.name}
                    </option>
                  );
                })}
              </select>
            </form>
          </div>
        </div>
        <div>
          <table className="table table-striped">
            <thead className="table table-dark">
              <tr>
                <td>Sales Person</td>
                <td>Automobile Vin</td>
                <td>Sold To</td>
                <td>Price</td>
              </tr>
            </thead>
            <tbody>
              {this.state.sales.map((sale) => {
                return (
                  <tr key={sale.id}>
                    <td>{sale.seller.name}</td>
                    <td>{sale.automobile.vin}</td>
                    <td>{sale.customer.name}</td>
                    <td>{sale.price}</td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
      </>
    );
  }
}

export default SalesBySeller;
