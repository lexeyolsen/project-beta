import React from "react";

class SalesRecordForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      automobiles: [],
      sellers: [],
      customers: [],
      price: "",
    };
    this.handleAutomobileChange = this.handleAutomobileChange.bind(this);
    this.handleSellerChange = this.handleSellerChange.bind(this);
    this.handleCustomerChange = this.handleCustomerChange.bind(this);
    this.handlePriceChange = this.handlePriceChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleAutomobileChange(event) {
    const value = event.target.value;
    this.setState({ automobile: value });
  }

  handleSellerChange(event) {
    const value = event.target.value;
    this.setState({ seller: value });
  }

  handleCustomerChange(event) {
    const value = event.target.value;
    this.setState({ customer: value });
  }

  handlePriceChange(event) {
    const value = event.target.value;
    this.setState({ price: value });
  }

  async handleSubmit(event) {
    event.preventDefault();
    const data = { ...this.state };

    delete data.automobiles;
    delete data.sellers;
    delete data.customers;

    const salesRecordUrl = "http://localhost:8090/api/list_sales/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(salesRecordUrl, fetchConfig);
    if (response.ok) {
      const cleared = {
        automobile: "",
        seller: "",
        customer: "",
        price: "",
      };
      this.setState(cleared);
    }
  }

  async componentDidMount() {
    const autoUrl = "http://localhost:8090/api/list_unsold_autos/";
    const salesPersonUrl = "http://localhost:8090/api/sales_people/";
    const customerUrl = "http://localhost:8090/api/potential_customers/";

    const autoResponse = await fetch(autoUrl);
    const salesPersonResponse = await fetch(salesPersonUrl);
    const customerResponse = await fetch(customerUrl);

    if (autoResponse.ok && salesPersonResponse.ok && customerResponse.ok) {
      const autoData = await autoResponse.json();

      const salesPersonData = await salesPersonResponse.json();

      const customerData = await customerResponse.json();

      this.setState({ automobiles: autoData.automobiles });
      this.setState({ sellers: salesPersonData.sellers });
      this.setState({ customers: customerData.customers });
    }
  }

  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a New Sales Record</h1>
            <form onSubmit={this.handleSubmit} id="create-location-form">
              <div className="mb-3">
                <select
                  onChange={this.handleAutomobileChange}
                  value={this.state.automobile}
                  required
                  name="automobile"
                  id="automobile"
                  className="form-select"
                >
                  <option value="">Choose an Automobile</option>
                  {this.state.automobiles.map((automobile) => {
                    return (
                      <option key={automobile.id} value={automobile.id}>
                        {automobile.vin}
                      </option>
                    );
                  })}
                </select>
              </div>
              <div className="mb-3">
                <select
                  onChange={this.handleSellerChange}
                  value={this.state.seller}
                  required
                  name="sales_person"
                  id="sales_person"
                  className="form-select"
                >
                  <option value="">Choose a Sales Person</option>
                  {this.state.sellers.map((seller) => {
                    return (
                      <option key={seller.id} value={seller.id}>
                        {seller.name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <div className="mb-3">
                <select
                  onChange={this.handleCustomerChange}
                  value={this.state.customer}
                  required
                  name="customer"
                  id="customer"
                  className="form-select"
                >
                  <option value="">Choose a Customer</option>
                  {this.state.customers.map((customer) => {
                    return (
                      <option key={customer.id} value={customer.id}>
                        {customer.name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={this.handlePriceChange}
                  value={this.state.price}
                  placeholder="Price"
                  required
                  type="number"
                  name="price"
                  id="price"
                  className="form-control"
                />
                <label htmlFor="price">Price</label>
              </div>
              <button className="btn btn-outline-success">Submit Sale</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default SalesRecordForm;
