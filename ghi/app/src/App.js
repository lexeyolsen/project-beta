import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import SalesPersonForm from './Sales/NewSalesPerson';
import PotentialCustomerForm from './Sales/NewPotentialCustomer';
import SalesRecordForm from './Sales/NewSalesRecord';
import TechnicianForm from './Service/NewTechnician';
import CreateServiceForm from './Service/CreateService';
import ServiceAppointments from './Service/CurrentServiceAppointments';
import Sales from './Sales/ListSales';
import SalesBySeller from './Sales/ListSalesBySeller';
import ServiceHistory from './Service/ServiceHistory';
import CreateManufacturer from './Inventory/CreateManufacturer';
import CreateVehicleModel from './Inventory/CreateVehicleModel';
import CreateAutomobile from './Inventory/CreateAutomobile';
import ManufacturesList from './Inventory/ManufacturesList';
import AutomobilesList from './Inventory/AutomobilesList';
import VehicleModelsList from './Inventory/VehicleModelsList';

function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>

          <Route path="/" element={<MainPage />} />

          <Route path="create-sales-person" element={<SalesPersonForm />} />
          <Route path="create-potential-customer" element={<PotentialCustomerForm />} />
          <Route path="create-sales-record" element={<SalesRecordForm />} />
          <Route path="list-sales" element={<Sales sales={props.sales} />} />
          <Route path="list-sales-by-seller" element={<SalesBySeller />} />


          <Route path="service-history" element={<ServiceHistory />} />
          <Route path="service-appointments" element={<ServiceAppointments services={props.services}/>} />
          <Route path="new-technician" element={<TechnicianForm />}/>
          <Route path="new-service" element={<CreateServiceForm />}/>


          <Route path="create-manufacturer" element={<CreateManufacturer />} />
          <Route path="create-vehicle-model" element={<CreateVehicleModel />} />
          <Route path="create-automobile" element={<CreateAutomobile />} />
          <Route path="manufacturers" element={<ManufacturesList manufacturers={props.manufacturers}/>}/>
          <Route path="automobiles" element={<AutomobilesList automobiles={props.automobiles}/>}/>
          <Route path="models" element={<VehicleModelsList models={props.models}/>}/>

        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
