import React from "react";

async function FinishedService(id) {
  if (
    window.confirm(
      "Are you sure you want to do this? IF THE WORK IS DONE!! I THINK A CARPESE SALAD IS IN YOUR FUTURE 🤪"
    )
  ) {
    const url = `http://localhost:8080/api/finished_services/${id}/`;
    const fetchConfig = {
      method: "PUT",
    };
    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      window.location.reload(false);
    }
  }
}

async function CancelAppointment(id) {
  if (
    window.confirm(
      "Are you sure you want to do this? WITH GREAT POWER COMES GREAT RESPOSIBLITY!!! 🤔"
    )
  ) {
    const url = `http://localhost:8080/api/services/${id}`;
    const fetchConfig = {
      method: "DELETE",
    };
    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      window.location.reload(false);
    } else {
      console.error(response);
    }
  }
}

function ServiceAppointments(props) {
  return (
    <div>
      <h1>Service Appointments</h1>
      <div>
        <table className="table table-striped">
          <thead className="table table-dark">
            <tr>
              <th>VIP?</th>
              <th>VIN</th>
              <th>Customer name</th>
              <th>Date / Time</th>
              <th>Technician</th>
              <th>Reason</th>
              <th>Cancel</th>
              <th>Finished</th>
            </tr>
          </thead>
          <tbody>
            {props.services?.map((service, i) => {
              return (
                <tr key={i}>
                  <td>{service.vip}</td>

                  <td>{service.vin}</td>
                  <td>{service.customer_name}</td>
                  <td>{service.DateTime}</td>
                  <td>{service.technician.name}</td>
                  <td>{service.reason}</td>
                  <td>
                    <button
                      className="btn btn-outline-danger me-2"
                      onClick={() => CancelAppointment(service.id)}
                    >
                      Cancel Service
                    </button>
                  </td>
                  <td>
                    <button
                      className="btn btn-outline-success me-2"
                      onClick={() => FinishedService(service.id)}
                    >
                      Service Finished
                    </button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    </div>
  );
}

export default ServiceAppointments;
