import React from "react";

class ServiceHistory extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      vin: "",
      service_history: [],
    };
  }

  async componentDidMount() {
    const url = "http://localhost:8080/api/completed_services/";
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      this.setState({ services: data.services });
    }
  }

  handleVinChange = (event) => {
    const value = event.target.value;
    this.setState({ vin: value });
  };

  handleSubmit = async (event) => {
    event.preventDefault();

    const history = this.state.services.filter((service) => {
      return service.vin === this.state.vin;
    });
    this.setState({ service_history: history });
  };

  render() {
    return (
      <>
        <div className="mt-4">
          <h1>Service History</h1>
        </div>
        <div className="row mb-4">
          <form
            className="d-flex"
            onSubmit={this.handleSubmit}
            id="appointments_history"
          >
            <input
              className="form-control me-2"
              onChange={this.handleVinChange}
              value={this.state.vin}
              type="text"
              placeholder="Search VIN"
              requiredtype="text"
              name="vin"
              id="vin"
            />
            <button className="btn btn-outline-success me-2" type="submit">
              Search
            </button>
          </form>
        </div>
        <table className="table table-striped">
          <thead className="table table-dark">
            <tr>
              <th>VIN</th>
              <th>Customer Name</th>
              <th>Appointment Date/Time</th>
              <th>Assigned Technician</th>
              <th>Service Reason</th>
            </tr>
          </thead>
          <tbody>
            {this.state.service_history
              ? this.state.service_history.map((history) => {
                  return (
                    <tr key={history.vin}>
                      <td>{history.vin}</td>
                      <td>{history.customer_name}</td>
                      <td>{history.DateTime}</td>
                      <td>{history.technician.name}</td>
                      <td>{history.reason}</td>
                    </tr>
                  );
                })
              : null}
          </tbody>
        </table>
      </>
    );
  }
}

export default ServiceHistory;
