import React from "react";

const initialState = {
  name: "",
  number: "",
};

class TechnicianForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = initialState;
  }

  handleSubmit = async (event) => {
    event.preventDefault();
    const data = { ...this.state };

    const technicianUrl = "http://localhost:8080/api/technicians/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(technicianUrl, fetchConfig);
    if (response.ok) {
    }
    this.setState(initialState);
  };

  handleNameChange = (event) => {
    const value = event.target.value;
    this.setState({ name: value });
  };
  handleNumberChange = (event) => {
    const value = event.target.value;
    this.setState({ number: value });
  };

  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a New Technician</h1>
            <form id="create-technician-form" onSubmit={this.handleSubmit}>
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleNameChange}
                  value={this.state.name}
                  placeholder="Name"
                  required
                  type="text"
                  name="name"
                  id="name"
                  className="form-control"
                />
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleNumberChange}
                  value={this.state.number}
                  placeholder="Number"
                  required
                  type="text"
                  name="number"
                  id="number"
                  className="form-control"
                />
                <label htmlFor="number">Employee #</label>
              </div>
              <button className="btn btn-outline-success">
                Create Technician
              </button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default TechnicianForm;
