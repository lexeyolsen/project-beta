import React from "react";

function VehicleModelsList(props) {
  return (
    <div className="pt-4">
      <h1>Vehicle Models</h1>
      <table className="table table-striped">
        <thead className="table table-dark">
          <tr>
            <th>Name</th>
            <th>Picture</th>
            <th>Manufacturer</th>
          </tr>
        </thead>
        <tbody>
          {props.models?.map((model, i) => {
            return (
              <tr key={i}>
                <td>{model.name}</td>
                <td>
                  <img
                    style={{ width: 100, height: 100 }}
                    src={model.picture_url}
                    alt="Car Picture"
                  />
                </td>
                <td>{model.manufacturer.name}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

export default VehicleModelsList;
