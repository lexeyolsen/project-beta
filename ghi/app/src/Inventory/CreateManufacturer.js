import React from "react";

class CreateManufacturer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
    };
    this.handleNameChange = this.handleNameChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleNameChange(event) {
    const value = event.target.value;
    this.setState({ name: value });
  }

  async handleSubmit(event) {
    event.preventDefault();
    const data = { ...this.state };
    const manufacturerUrl = "http://localhost:8100/api/manufacturers/";

    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const manufacturerResponse = await fetch(manufacturerUrl, fetchConfig);

    if (manufacturerResponse.ok) {
      const cleared = {
        name: "",
      };

      this.setState(cleared);
    }
  }

  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add Car Manufacturer</h1>
            <form onSubmit={this.handleSubmit} id="create-manufacturer">
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleNameChange}
                  value={this.state.name}
                  placeholder="Name"
                  required
                  type="text"
                  name="name"
                  id="name"
                  className="form-control"
                />
                <label htmlFor="name">Name</label>
              </div>
              <button className="btn btn-outline-success">
                Add Manufacturer
              </button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default CreateManufacturer;
