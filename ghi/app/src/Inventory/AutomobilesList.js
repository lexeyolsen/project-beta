import React from "react";

function AutomobilesList(props) {
  return (
    <div className="pt-4">
      <h1>Automobiles</h1>
      <table className="table table-striped">
        <thead className="table table-dark">
          <tr>
            <th>VIN</th>
            <th>Color</th>
            <th>Year</th>
            <th>Model</th>
            <th>Manufacturer</th>
          </tr>
        </thead>
        <tbody>
          {props.automobiles?.map((automobile, i) => {
            return (
              <tr key={i}>
                <td>{automobile.vin}</td>
                <td>{automobile.color}</td>
                <td>{automobile.year}</td>
                <td>{automobile.model.name}</td>
                <td>{automobile.model.manufacturer.name}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

export default AutomobilesList;
