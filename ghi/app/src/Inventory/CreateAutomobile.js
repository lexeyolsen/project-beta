import React from "react";

class CreateAutomobile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      color: "",
      year: "",
      vin: "",
      models: [],
      model_id: "",
    };

    this.handleColorChange = this.handleColorChange.bind(this);
    this.handleYearChange = this.handleYearChange.bind(this);
    this.handleVinChange = this.handleVinChange.bind(this);
    this.handleModelChange = this.handleModelChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleColorChange(event) {
    const value = event.target.value;
    this.setState({ color: value });
  }

  handleYearChange(event) {
    const value = event.target.value;
    this.setState({ year: value });
  }

  handleVinChange(event) {
    const value = event.target.value;
    this.setState({ vin: value });
  }

  handleModelChange(event) {
    const value = event.target.value;
    this.setState({ model_id: value });
  }

  async handleSubmit(event) {
    event.preventDefault();
    const data = { ...this.state };
    delete data.models;

    const autoUrl = "http://localhost:8100/api/automobiles/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const autoResponse = await fetch(autoUrl, fetchConfig);

    if (autoResponse.ok) {
      const cleared = {
        color: "",
        year: "",
        vin: "",
        model_id: "",
      };

      this.setState(cleared);
    }
  }

  async componentDidMount() {
    const modelUrl = "http://localhost:8100/api/models/";

    const modelResponse = await fetch(modelUrl);

    if (modelResponse.ok) {
      const data = await modelResponse.json();

      this.setState({ models: data.models });
    }
  }

  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add Automobile</h1>
            <form onSubmit={this.handleSubmit} id="create-automobile">
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleColorChange}
                  value={this.state.color}
                  placeholder="Color"
                  required
                  type="text"
                  name="color"
                  id="color"
                  className="form-control"
                />
                <label htmlFor="color">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleYearChange}
                  value={this.state.year}
                  placeholder="Year"
                  required
                  type="number"
                  name="year"
                  id="year"
                  className="form-control"
                />
                <label htmlFor="year">Year</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleVinChange}
                  value={this.state.vin}
                  placeholder="Vin"
                  required
                  type="text"
                  name="vin"
                  id="vin"
                  className="form-control"
                />
                <label htmlFor="vin">Vin</label>
              </div>
              <div className="mb-3">
                <select
                  onChange={this.handleModelChange}
                  value={this.state.model_id}
                  required
                  name="model"
                  id="model"
                  className="form-select"
                >
                  <option value="">Choose a Model</option>
                  {this.state.models.map((model) => {
                    return (
                      <option key={model.id} value={model.id}>
                        {model.name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-outline-success">
                Add Automobile
              </button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default CreateAutomobile;
