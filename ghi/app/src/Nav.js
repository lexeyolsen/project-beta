import { NavLink } from "react-router-dom";

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">
          CarCar
        </NavLink>
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="pt-2 nav-item dropdown">
              <a
                className="nav-link dropdown-toggle text-light"
                href="#"
                id="navbarDropdown"
                role="button"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                Vehicles
              </a>
              <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                <NavLink
                  className="dropdown-item"
                  aria-current="page"
                  to="/manufacturers/"
                >
                  Manufacturers
                </NavLink>
                <NavLink
                  className="dropdown-item text-dark"
                  aria-current="page"
                  to="/automobiles/"
                >
                  Automobiles
                </NavLink>
                <NavLink
                  className="dropdown-item text-dark"
                  aria-current="page"
                  to="/models/"
                >
                  Models
                </NavLink>
                <NavLink
                  className="dropdown-item text-dark"
                  aria-current="page"
                  to="/create-manufacturer/"
                >
                  Add Manufacturer
                </NavLink>
                <NavLink
                  className="dropdown-item text-dark"
                  aria-current="page"
                  to="create-vehicle-model"
                >
                  Add Car Model
                </NavLink>

                <NavLink
                  className="dropdown-item text-dark"
                  aria-current="page"
                  to="/create-automobile/"
                >
                  Add Automobile
                </NavLink>
              </ul>
            </li>
            <li className="pt-2 nav-item dropdown">
              <a
                className="nav-link dropdown-toggle text-light"
                href="#"
                id="navbarDropdown"
                role="button"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                Sales
              </a>
              <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                <NavLink
                  className="dropdown-item text-dark"
                  aria-current="page"
                  to="/list-sales/"
                >
                  Sales
                </NavLink>
                <NavLink
                  className="dropdown-item text-dark"
                  aria-current="page"
                  to="/list-sales-by-seller/"
                >
                  Sales by Seller
                </NavLink>
                <NavLink
                  className="dropdown-item text-dark"
                  aria-current="page"
                  to="/create-potential-customer/"
                >
                  Add Customer
                </NavLink>
                <NavLink
                  className="dropdown-item"
                  aria-current="page"
                  to="/create-sales-person/"
                >
                  Add Seller
                </NavLink>
                <NavLink
                  className="dropdown-item text-dark"
                  aria-current="page"
                  to="/create-sales-record/"
                >
                  Add Sale
                </NavLink>
              </ul>
            </li>
            <li className="pt-2 nav-item dropdown">
              <a
                className="nav-link dropdown-toggle text-light"
                href="#"
                id="navbarDropdown"
                role="button"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                Service
              </a>
              <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                <NavLink
                  className="dropdown-item text-dark"
                  aria-current="page"
                  to="/service-appointments/"
                >
                  Current Appointments
                </NavLink>
                <NavLink
                  className="dropdown-item text-dark"
                  aria-current="page"
                  to="/service-history/"
                >
                  Services by Vin
                </NavLink>
                <NavLink
                  className="dropdown-item"
                  aria-current="page"
                  to="/new-technician/"
                >
                  Add Technician
                </NavLink>
                <NavLink
                  className="dropdown-item text-dark"
                  aria-current="page"
                  to="/new-service/"
                >
                  Schedule a Service
                </NavLink>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
}

export default Nav;
