# CarCar by Dean and Lexey
![](images/HRLOGO.jpg)

## Assigned Components
![](images/APP%20Breakdown.png)

## Application Map
![](images/boundedcontext.png)

## Running the Application
<details><summary>HelpFul terminal commands</summary>

To create a directory, use: `mkdir <<directory-name>>`

To see the files and directories inside your current directory, use: `ls`

To go into a directory inside your current directory, use: `cd <<directory-name>>`

To back out one livel of your directory, use: `cd ..`

To clone a repo, use: `git clone <<file-link>>`

To open a directory in VS Code, use: `code .`

</details>

**To run the Application:**

1. Copy the repo's HTTPS clone link

![](images/https.jpg)

2. Open your computers terminal

3. Using `cd` and `mkdir` , navigate to the directory you want the project located in

4. Use `git clone` to clone the repo into your current directory

5. `cd` into the cloned project

6. Open the project in VS Code with `code .`

7. Open up the docker computer application as well

8. Open a terminal in VS Code

9. In the terminal, run the command `docker volume create beta-data` to create the needed volume

10. Then run `Docker compose build` to build the docker containers

11. Then run `Docker compose up` to run the containers

12. Wait for a undetermined amount of time for the React sever to spool up

![](images/waiting.png)

13. Open Chrome to a new tab, in the address bar, navigate to "localhost:3000"

14. Find a snack and browse the application

## Service microservice


With-in the Service microservice you start by browsing your current appoinments which will also list out if a upcoming appoinment was a returning cusomter of the sales department by using the VIN to make and a api call to the inventory api sharing VIN information to allow the VIP status to be made.
Next you are able to search by using any completed appoinments using the VIN to make sure all results are speicific to a particular customers vehilce.
Then you are able to add a Technician to your team using the Add Technician feature providing a Name and ID number sepcific to that Technician.
Last you can schedule an appoinment for a customer to visit your repair department for any concerns they may have.

There are 3 models inside the service microservice they are:

AutomovileVO Model which was created for getting the data from the inventory api by calling for the VIN information.

Technician Model which was created to handle the creation of a new Technician by providing name and an employee ID.

Service Model which was created for handeling all the required information accociated to creation of a new appointment keeping track multiple data points. vip?, VIN, customers name, Date and Time of appointment, reason for appointment, status of appointment, and technican assigned to appointment.

<details><summary>HTTP Service Requests for Insomnia</summary>

**Technicians:**

JSON needed for Create Technician request:
```
{
    "name":
    "employee_number": (expects a number)
}
```
List Technicians (GET):
http://localhost:8080/api/technicians/

Create Technician (POST):
http://localhost:8080/api/technicians/

**Services:**

List Services (GET):
http://localhost:8080/api/services/

**Completed Services:**

List Completed Services (GET):
http://localhost:8080/api/completed_services/


</details>



## Sales microservice

Inside the Sales microservice, the models include AutomobileVO, SalesPerson, PotentialCustomer, and SalesRecord.

 The AutomobileVO utilizes the poller in the sales microservice, to pull new necessary automobile data.

The SalesRecord model has foreign key properties to connect a sale to a automobile, seller, and the customer it was sold to, and also includes a price property.

Utilizing views and encoders, I was able to make the neccesary requests go gather and create the data needed, to start working on the front end of the application.

The forms allow you to create a customer, a seller, and a sale. Utilizing RESTful api's I was able to fetch the neccessary data to populate the lists from the data created in the forms.

To start creating your own data, you can either use the application on it's own, or you can use Insomnia. The needed infomation for Insomnia is included below!

<details><summary>HTTP Sales Requests for Insomnia</summary>

**Autos:**

JSON needed for Create Automobile request:
```
{
    "color":
    "year": (expects a number)
    "vin":
}
```
List Automobiles (GET):
http://localhost:8090/api/list_autos/

List Sold Automobiles (GET):
http://localhost:8090/api/list_unsold_autos/

Create Automobile (POST):
http://localhost:8090/api/list_autos/





**Sellers:**

JSON needed for Create Seller Request:
```
{
    "name":
    "number":
}
```

List Sellers (GET):
http://localhost:8090/api/sales_people/

Create Seller (post):
http://localhost:8090/api/sales_people/





**Potential customers:**

Make sure to insert customer id inbetween "//" on show potential customer request

JSON needed for Create Potential Customer request:
```
{
    "name":
    "address":
    "phone_number":
}
```

List Potential Customers (GET):
http://localhost:8090/api/potential_customers/

Show Potential Customer (GET):
http://localhost:8090/api/potential_customer//

Create Potential Customer (POST):
http://localhost:8090/api/potential_customers/





* Sales Records

Make sure to insert seller id inbetween "//" on list sales by seller request

JSON needed for Create Sales Record request:
```
{
    "automobile":
    "seller":
    "customer":
    "price": (expects a number)
}
```

List Sales (GET):
http://localhost:8090/api/list_sales/

List Sales by Seller (GET):
http://localhost:8090/api/list_sales//

Create Sales Record (POST):
http://localhost:8090/api/list_sales/

</details>
