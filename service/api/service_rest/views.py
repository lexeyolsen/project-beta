from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
from django.http import JsonResponse
import json
from .models import AutomobileVO, Service, Technician
from datetime import datetime


# Create your views here.
class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "color",
        "year",
        "vin",
    ]

class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties =[
        "name",
        "number",
        "id",

    ]

class ServiceEncoder(ModelEncoder):
    model = Service
    properties = [
        "id",
        "vin",
        "customer_name",
        "DateTime",
        "technician",
        "reason",
        "status",
        "vip",
    ]
    encoders = {
        "technician": TechnicianEncoder(),
    }

    def get_extra_data(self, o):
        try:
            AutomobileVO.objects.get(vin=o.vin)
            return {"vip": "Yes",
            "DateTime": o.DateTime.strftime("%m-%d-%Y at %I:%M:%p")}

        except:
            return {"vip": "No",
            "DateTime": o.DateTime.strftime("%m-%d-%Y at %I:%M:%p")}




@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder= TechnicianEncoder,
            safe = False
            )
    else:
        content = json.loads(request.body)
        technician = Technician.objects.create(**content)
        return JsonResponse(
            technician,
            encoder= TechnicianEncoder,
            safe = False
        )

@require_http_methods(["DELETE"])
def api_delete_technicians(request, pk):
    if request.method == "DELETE":
        count, _ = Technician.objects.filter(id=pk).delete()
        return JsonResponse({
            "deleted": count > 0
        })


@require_http_methods(["GET", "POST"])
def api_list_services(request):
    if request.method == "GET":
        services = Service.objects.all()
        return JsonResponse(
            {'services': services},
            encoder = ServiceEncoder,
            safe= False

        )
    else:
        content = json.loads(request.body)
        if "technician" in content:
            try:
                technician = Technician.objects.get(number = content["technician"])
                content["technician"] = technician
            except Technician.DoesNotExist:
                return JsonResponse({
                   {"message": "Not a Technician"}
                })
        service = Service.objects.create(**content)
        return JsonResponse(
            service,
            encoder= ServiceEncoder,
            safe = False
        )

@require_http_methods(["DELETE"])
def api_delete_services(request, pk):
    if request.method == "DELETE":
        count, _ = Service.objects.filter(id=pk).delete()
        return JsonResponse({
            "deleted": count > 0
        })

@require_http_methods(["GET"])
def api_unfinished_services(request):
    if request.method == "GET":
        unfinished = Service.objects.filter(status="False")
        return JsonResponse(
            {"services": unfinished},
            encoder= ServiceEncoder,
            )


@require_http_methods(["PUT"])
def api_finished_services(request, pk):
    if request.method == "PUT":
        finished = Service.objects.filter(id=pk).update(status="True")
        return JsonResponse(
            {"services": finished},
            encoder = ServiceEncoder,
    )


@require_http_methods(["GET"])
def api_completed_services(request):
    if request.method == "GET":
        completed = Service.objects.filter(status=True)
        return JsonResponse(
            {"services":completed},
            encoder= ServiceEncoder,
            safe=False
        )
