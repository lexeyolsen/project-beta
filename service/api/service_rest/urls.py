from django.urls import path
from .views import (
    api_list_services,
    api_list_technicians,
    api_delete_technicians,
    api_delete_services,
    api_unfinished_services,
    api_finished_services,
    api_completed_services,
    )

urlpatterns = [
    path("services/", api_list_services, name="api_list_services") ,
    path("services/<int:pk>/", api_delete_services, name="api_delete_services"),
    path("unfinished_services/", api_unfinished_services, name="api_unfinished_services"),
    path("technicians/", api_list_technicians, name="api_list_technicians"),
    path("technicians/<int:pk>/", api_delete_technicians, name="api_delete_technicians"),
    path("finished_services/<int:pk>/", api_finished_services, name="finished_services"),
    path("completed_services/", api_completed_services, name="completed_services"),
]
